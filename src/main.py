from datetime import datetime
import requests as r
from bs4 import BeautifulSoup as bs
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import logging

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG) 

class Html_Elements:
    def __init__(self, *args):
        pass

class Scraper:

    def __init__(self, url, css_selector = None) -> None:
        self.url = url
        self.css_selector = css_selector
        self.html = ""

    def get_html(self):
        html = r.get(self.url, allow_redirects=True)
        self.html = html.text if html.status_code == 200 else "No data"

        logging.info(self.html.title)
        
    
    def parse_html_by_css_selector(self):
        self.html_parsed = bs(self.html, 'html.parser').select(self.css_selector)
    
class Mail_Provider:
    def __init__(self, sender, password, receiver, subject, body):
        self.sender = sender
        self.password = password
        self.receiver = receiver
        self.subject = subject
        self.body = body
        self.message = ""
    
    def build_message(self):
        message = MIMEMultipart()
        message["from"] = self.sender
        message["to"] = self.receiver
        message["subject"] = self.subject

        message.attach(MIMEText(self.body, "html"))

        self.message = message.as_string()

    
    def send_mail(self):
        with smtplib.SMTP_SSL('smtp.gmail.com', 465, context = ssl.create_default_context()) as server:
            server.login(self.sender, self.password)
            errors = server.sendmail(self.sender, self.receiver, self.message)

            logger.info("Successfully delivered") if len(errors) == 0 else logger.error(f"Something terrible happend while sending mail. Errors: {errors}")

def run():
    scraper = Scraper("https://eumetsat.jobbase.io", 'div[class="cell-table col-sm-17 col-xs-20"] > div > h3 > a')

    scraper.get_html()
    scraper.parse_html_by_css_selector()

    vacancies, file_links = [v.text for v in scraper.html_parsed], [(scraper.url + f['href']) for f in scraper.html_parsed]

    table_html = [f"<tr> <td> {i[0]} </td> <td> {i[1]} </td> </tr>" for i in tuple(zip(vacancies, file_links))]

    scraper_kit = Scraper("https://www.careerserviceportal.kit.edu/en/jobboerse/?s=1&search_text=&search_arr_filter_2=&search_arr_filter_7=&search_arr_filter_3=&search_arr_filter_6=&search_arr_filter_8=xz&search_arr_filter_9=&search_submit=Search")
    scraper_kit.get_html()

    soup_ecmwf = bs(scraper_kit.html, 'html.parser')

    jobs_ecmwf = []

    for job_row in soup_ecmwf.find_all('tr', class_='pack_2'):
        job_title_link = job_row.find('div', class_='ov-job-flbx').find('div', class_='ov-jf').find('div', class_='joblink').find('a')
        job_title = job_title_link.get_text(strip=True)
        # pdf_link = job_row.find('div', class_='ov-job-flbx').find('div', class_='ov-gfx').find('div', class_='ov-addycont').find('div', class_='job-pdf-ico').find('a')['href']
        job_type = job_row.find('div', class_='ov-job-flbx').find('div', class_='ov-jf').find_all('br')[1].previous_sibling.text
        institute = job_row.find('div', class_='ov-job-flbx').find('div', class_='ov-jf').find('span', class_='comp-link').get_text(strip=True)

        jobs_ecmwf.append({
            'job_title': job_title,
            # 'pdf_link': pdf_link,
            'job_type': job_type,
            'institute': institute
        })

    print(jobs_ecmwf)
    table_html_another_company = []
    for job in jobs_ecmwf:
        row_html = f"""
        <tr>
            <td>{job['job_title']}</td>
            <td>{job['job_type']}</td>
            <td>{job['institute']}</td>
        </tr>
        """
# <td><a href="{job['pdf_link']}">Link</a></td>
            
    
        table_html_another_company.append(row_html)

    msg ="<!DOCTYPEhtml><html><head><style>table{font-family:arial,sans-serif;}td,th{border:1pxsolid#dddddd;text-align:left;padding:8px;}tr:nth-child(even){background-color:#dddddd;}</style></head>"

    msg_body = "<body><h2>Available Positions at EUMETSAT</h2><p>You'll be a part of this someday, so keep working every day!</p>  <table>   <tr>     <th>Position</th>     <th>Link</th>  %s  </table>      <hr> <!-- Add a horizontal line to separate companies --> <h2>Available Positions at KIT</h2> <p>Description of the other company...</p> <table> <tr> <th>Position</th> </tr> %s </table></body> </html>" % ("".join(table_html), "".join(table_html_another_company))

    msg += msg_body

    mail_provider = Mail_Provider("jobupdatesfromeumetsat@gmail.com", 
                                  "scjpagonkjqozalo", 
                                  "berkesenturk11@gmail.com", 
                                  "EUMETSAT Vacancies on {}".format(datetime.today().strftime("%Y/%m/%d")), 
                                  msg)



    mail_provider.build_message()
    mail_provider.send_mail()

if __name__ == "__main__":
    try:
        run()
    except Exception as ex:
        logger.error(ex)